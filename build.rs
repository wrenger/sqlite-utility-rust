use std::process::Command;

fn main() {
    // Compile Gresource
    let out = Command::new("glib-compile-resources")
        .args(&["--generate", "sqlite-utility.gresource.xml"])
        .current_dir("resources")
        .status()
        .expect("failed to generate resources");
    assert!(out.success());
}
