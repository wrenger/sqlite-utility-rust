# SQLite Utility

A Lightweight Tool for working with SQLite Databases written in Rust.

This is a demo project for building [GTK+3](https://www.gtk.org/) GUIs
with [Glade](https://glade.gnome.org/) and
[Gtk-rs](https://gtk-rs.org/).

## Requirements

* cargo
* libgtk-3-dev

## Setup Build Environment

First follow the steps on
[Rust Getting Started](https://www.rust-lang.org/learn/get-started)
to install Rust.

**To build and execute run:**

```bash
cargo run
```

**To create a release build run:**

```bash
cargo build --release
```

### Cross build

These steps has to be executed only on the first time:
```bash
# Install crossbuild (see https://github.com/rust-embedded/cross#dependencies)
cargo install cross
# Build the docker container
docker build -t custom/cross-win-sqlite-utility docker/cross-win
```

> Enter the docker container (useful for troubleshooting & debugging)
> `docker run --rm -it --volume=$(pwd):/home/docker/project custom/cross-win-sqlite-utility bash`

Compiling for windows:
```bash
cross build --target=x86_64-pc-windows-gnu --release
```

Packaging:
```bash
docker run --rm -it --volume=$(pwd):/home/docker/project custom/cross-win-sqlite-utility /home/docker/project/docker/cross-win/package.sh
```

------

# See Also

* [GTK+3](https://www.gtk.org/)
* [Glade](https://glade.gnome.org/)
* [Gtk-rs](https://gtk-rs.org/)
