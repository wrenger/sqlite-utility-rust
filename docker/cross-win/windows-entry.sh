#!/usr/bin/env bash

# See https://github.com/rust-embedded/cross

set -e

export HOME=/tmp/home
mkdir -p "${HOME}"

# Initialize the wine prefix (virtual windows installation)
export WINEPREFIX=/tmp/wine
mkdir -p "${WINEPREFIX}"
# FIXME: Make the wine prefix initialization faster
wineboot &> /dev/null

# Put libstdc++ and some other mingw dlls in WINEPATH
export WINEPATH="/usr/x86_64-w64-mingw32/sys-root/mingw/bin"

exec "$@"
