SYSROOT="/usr/x86_64-w64-mingw32/sys-root/mingw"
PROJECT_PATH="/home/docker/project"
PACKAGE_PATH="$PROJECT_PATH/win/release"

mkdir -p $PACKAGE_PATH
# Executables & libs
cp $PROJECT_PATH/target/x86_64-pc-windows-gnu/release/*.exe $PACKAGE_PATH
cp $SYSROOT/bin/*.dll $PACKAGE_PATH
# Schemas
mkdir -p $PACKAGE_PATH/share/glib-2.0/schemas
cp $SYSROOT/share/glib-2.0/schemas/* $PACKAGE_PATH/share/glib-2.0/schemas
# Icons
mkdir -p $PACKAGE_PATH/share/icons
cp -r $SYSROOT/share/icons/* $PACKAGE_PATH/share/icons
