//! # SQLite Utility
//!
//! A Lightweight Tool for working with SQLite Databases written in Rust.
//!
//! This is a demo project for building [GTK+3](https://www.gtk.org/) GUIs
//! with [Glade](https://glade.gnome.org/) and
//! [Gtk-rs](https://gtk-rs.org/).

use gio::prelude::*;
use gtk::prelude::*;

use std::env::args;

mod database;
mod resources;
mod window;

use resources::{UIAboutResource, UIMenuResource, UIResource, UIWindowResource};

const PKG_NAME: &str = env!("CARGO_PKG_NAME");
const PKG_VERSION: &str = env!("CARGO_PKG_VERSION");
const PKG_REPOSITORY: &str = env!("CARGO_PKG_REPOSITORY");
const PKG_AUTHORS: &str = env!("CARGO_PKG_AUTHORS");
const PKG_DESCRIPTION: &str = env!("CARGO_PKG_DESCRIPTION");

fn main() {
    let app = gtk::Application::new(Some("com.wrenger.sbv"), gio::ApplicationFlags::FLAGS_NONE)
        .expect("Failed to init Application");

    resources::register().expect("Failed to Register Resource Bundles.");

    app.set_accels_for_action("app.quit", &["<Ctrl>Q"]);

    app.connect_startup(move |app| {
        let window_res = UIWindowResource::load().unwrap();
        let window =
            window::MainWindow::new(&app, window_res).expect("Failed to create MainWindow.");

        // Creating action for displaying the about dialog.
        {
            let action = gio::SimpleAction::new("about", None);
            let window = window.clone();
            action.connect_activate(move |_, _| {
                let about_res = UIAboutResource::load().unwrap();
                let dialog = about_res.about_dialog;
                dialog.set_program_name(PKG_NAME);
                dialog.set_version(Some(PKG_VERSION));
                dialog.set_website(Some(PKG_REPOSITORY));
                dialog.set_website_label(Some("Repository"));
                dialog.set_authors(&PKG_AUTHORS.split(';').collect::<Vec<_>>());
                dialog.set_comments(Some(PKG_DESCRIPTION));

                if let Ok(window) = &window.try_borrow() {
                    dialog.set_transient_for(Some(window.win()));
                }
                dialog.run();
                dialog.close();
            });
            app.add_action(&action);
        }

        let menu_res = UIMenuResource::load().unwrap();
        app.set_app_menu(Some(&menu_res.menu));

        // Every time the app is activated.
        app.connect_activate(move |_| {
            if let Ok(window) = window.try_borrow() {
                window.present();
            }
        });
    });

    app.run(&args().collect::<Vec<_>>());
}
