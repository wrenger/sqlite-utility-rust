use std::fmt;
use std::path::{Path, PathBuf};

/// Handles the database access.
pub struct Database {
    path: PathBuf,
    connection: sqlite::Connection,
}

impl Database {
    /// Connect to a new database
    pub fn new(path: PathBuf) -> Result<Database, sqlite::Error> {
        let connection = sqlite::open(path.clone())?;
        Ok(Database { path, connection })
    }

    /// Executes the sql statement and provides the result to the given callback.
    pub fn execute<T, F>(&self, statement: T, callback: F) -> Result<(), sqlite::Error>
    where
        T: AsRef<str>,
        F: FnMut(&[(&str, Option<&str>)]) -> bool,
    {
        self.connection.iterate(statement, callback)
    }

    /// Executes the sql query and returns the result.
    pub fn fetch<T>(&self, statement: T) -> Result<Vec<Vec<String>>, sqlite::Error>
    where
        T: AsRef<str>,
    {
        let mut result = vec![];

        self.execute(statement, |pairs| {
            result.push(
                pairs
                    .iter()
                    .map(|&(_, value)| value.unwrap_or_default().into())
                    .collect(),
            );
            true
        })?;
        Ok(result)
    }

    pub fn get_path(&self) -> &Path {
        &self.path
    }
}

impl fmt::Debug for Database {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Database {{ {:?} }}", self.path)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn connection() {
        Database::new(":memory:".into()).unwrap();
    }

    #[test]
    fn fetch() {
        let db = Database::new(":memory:".into()).unwrap();
        db.execute("create table abc (a, b, c)", |_| false).unwrap();
        db.execute("insert into abc values ('a', 'b', 'c')", |_| false)
            .unwrap();

        let result = db.fetch("select * from abc").unwrap();
        assert_eq!(
            result,
            vec![vec![
                String::from("a"),
                String::from("b"),
                String::from("c")
            ]]
        );
    }
}
