use std::cell::RefCell;
use std::path::{Path, PathBuf};
use std::rc::Rc;

use gio::ActionMapExt;
use gtk::prelude::*;

use crate::database;
use crate::resources::UIWindowResource;

/// make moving clones into closures more convenient
macro_rules! clone {
    (@param _) => ( _ );
    (@param $x:ident) => ( $x );
    ($($n:ident),+ => move || $body:expr) => (
        {
            $( let $n = $n.clone(); )+
            move || $body
        }
    );
    ($($n:ident),+ => move |$($p:tt),+| $body:expr) => (
        {
            $( let $n = $n.clone(); )+
            move |$(clone!(@param $p),)+| $body
        }
    );
}

/// Creates an file open dialog for sqlite databases.
fn db_open_dialog(window: &gtk::Window, directory: Option<&Path>) -> Option<PathBuf> {
    let dialog = gtk::FileChooserDialog::with_buttons(
        Some("Open Database"),
        Some(window),
        gtk::FileChooserAction::Open,
        &[
            ("Cancel", gtk::ResponseType::Cancel),
            ("Open", gtk::ResponseType::Ok),
        ],
    );
    let filter = gtk::FileFilter::new();
    filter.set_name(Some("SQLite DB"));
    filter.add_mime_type("application/x-sqlite3");
    dialog.add_filter(&filter);

    let filter = gtk::FileFilter::new();
    filter.set_name(Some("Any Files"));
    filter.add_pattern("*");
    dialog.add_filter(&filter);

    if let Some(directory) = directory {
        dialog.set_current_folder(directory);
    }

    dialog.run();
    let file = dialog.get_filename();
    dialog.close();
    file
}

/// Creates an file create dialog for selecting a location and name for a new
/// sqlite databases.
fn db_new_dialog(window: &gtk::Window) -> Option<PathBuf> {
    let dialog = gtk::FileChooserDialog::with_buttons(
        Some("Create a new Database"),
        Some(window),
        gtk::FileChooserAction::Save,
        &[
            ("Cancel", gtk::ResponseType::Cancel),
            ("New", gtk::ResponseType::Ok),
        ],
    );

    dialog.run();
    let file = dialog.get_filename();
    dialog.close();
    file
}

/// Stores all gtk widgets that have to be updated during the lifetime of
/// this window together with additional sate.
#[derive(Debug)]
pub struct MainWindow {
    // widgets
    window: gtk::ApplicationWindow,
    header_bar: gtk::HeaderBar,
    command_field: gtk::TextView,
    table_select: gtk::ComboBoxText,
    result_table: gtk::TreeView,
    status_label: gtk::Label,
    // sate
    db: Option<database::Database>,
}

impl MainWindow {
    /// Initializing the main application window.
    ///
    /// Setup the content and connect the signals.
    pub fn new(
        app: &gtk::Application,
        window_res: UIWindowResource,
    ) -> Result<Rc<RefCell<MainWindow>>, ()> {
        // load objects
        let UIWindowResource {
            main_window,
            header_bar,
            command_field,
            table_select,
            result_table,
            status,
            new,
            open,
            schema,
            execute,
        } = window_res;

        main_window.set_application(Some(app));

        let win = Rc::new(RefCell::new(MainWindow {
            window: main_window,
            header_bar,
            command_field,
            table_select,
            result_table,
            status_label: status,
            db: None,
        }));

        // init signals

        new.connect_clicked(clone!(win => move |_| {
            win.try_borrow_mut().ok().map_or((), |mut w| w.on_new())
        }));

        open.connect_clicked(clone!(win => move |_| {
            win.try_borrow_mut().ok().map_or((), |mut w| w.on_open())
        }));

        schema.connect_clicked(clone!(win => move |_| {
            win.try_borrow().ok().map_or((), |w| w.on_schema())
        }));

        execute.connect_clicked(clone!(win => move |_| {
            win.try_borrow().ok().map_or((), |w| w.on_execute())
        }));

        win.borrow()
            .table_select
            .connect_changed(clone!(win => move |_| {
                // Try borrow is important to prevent deadlocks when updating
                // combo box items from within the window!
                win.try_borrow().ok().map_or((), |w| w.on_table_select())
            }));

        let action = gio::SimpleAction::new("new", None);
        action.connect_activate(clone!(win => move |_, _| {
            win.try_borrow_mut().ok().map_or((), |mut w| w.on_new())
        }));
        win.borrow().window.add_action(&action);

        let action = gio::SimpleAction::new("open", None);
        action.connect_activate(clone!(win => move |_, _| {
            win.try_borrow_mut().ok().map_or((), |mut w| w.on_open())
        }));
        win.borrow().window.add_action(&action);

        let action = gio::SimpleAction::new("close", None);
        action.connect_activate(clone!(win => move |_, _| {
            win.try_borrow_mut().ok().map_or((), |mut w| w.close())
        }));
        win.borrow().window.add_action(&action);

        win.borrow().load_tables();

        Ok(win)
    }

    /// Close the window and possibly opened database.
    pub fn close(&mut self) {
        self.db = None; // close db
        self.window.close();
    }

    /// Show the window.
    pub fn present(&self) {
        self.window.present()
    }

    /// Get the underlying gtk window.
    pub fn win(&self) -> &gtk::ApplicationWindow {
        &self.window
    }

    /// Updates the table selector combo box.
    fn load_tables(&self) {
        self.table_select.remove_all();
        // placeholder item
        self.table_select.append_text("<Table>");
        if let Some(db) = &self.db {
            if let Ok(result) = db.fetch("select name from sqlite_master WHERE type=\"table\"") {
                for row in result {
                    self.table_select.append_text(&row[0]);
                }
            }
        }
        // Just select the first element of the combo box.
        self.table_select.set_active_iter(
            self.table_select
                .get_model()
                .and_then(|iter| iter.get_iter_first())
                .as_ref(),
        );
    }

    /// Connect to a new database.
    fn connect(&mut self, path: PathBuf) {
        self.db = None;

        match database::Database::new(path.clone()) {
            Ok(db) => {
                self.db = Some(db);
                self.header_bar.set_subtitle(path.to_str());
            }
            Err(err) => {
                self.status_label.set_text(&format!("Error: {}", err));
                self.header_bar.set_subtitle(None);
            }
        }

        self.load_tables();
    }

    /// Initializes the result table according to the result of the sql query.
    fn init_result_table(&self, pairs: &[(&str, Option<&str>)]) {
        for (i, &(column, _)) in pairs.iter().enumerate() {
            let renderer = gtk::CellRendererText::new();
            let col = gtk::TreeViewColumn::new();
            col.pack_start(&renderer, true);
            col.add_attribute(&renderer, "text", i as i32);
            col.set_title(column);
            col.set_sort_indicator(true);
            col.set_sort_column_id(i as i32);
            col.set_resizable(true);
            col.set_min_width(20);

            // Callback for highlighting 'NULL' values.
            TreeViewColumnExt::set_cell_data_func(
                &col,
                &renderer,
                Some(Box::new(move |_, renderer, model, iter| {
                    let renderer: &gtk::CellRendererText = renderer.downcast_ref().unwrap();
                    match model.get_value(iter, i as i32).get::<&str>() {
                        Ok(Some(_)) => {
                            renderer.set_property_foreground(None);
                        }
                        _ => {
                            renderer.set_property_text(Some("null"));
                            renderer.set_property_foreground(Some("grey"));
                        }
                    }
                })),
            );

            self.result_table.append_column(&col);
        }
    }

    /// Executes a sql statement and displays its result.
    fn execute(&self, statement: &str) {
        // Clear table
        self.result_table.set_model(None as Option<&gtk::TreeModel>);
        for col in self.result_table.get_columns() {
            self.result_table.remove_column(&col);
        }

        if let Some(db) = &self.db {
            let mut model: Option<gtk::ListStore> = None;

            // Process the query result.
            db.execute(statement, |pairs| {
                if model.is_none() {
                    self.init_result_table(pairs);
                    model = Some(gtk::ListStore::new(&vec![glib::Type::String; pairs.len()]));
                }

                if let Some(model) = &model {
                    let pos = model.append();
                    for (i, &(_, value)) in pairs.iter().enumerate() {
                        model.set_value(&pos, i as u32, &value.into());
                    }
                }
                true
            })
            .map(|_| {
                // Update the result table content.
                if let Some(model) = &model {
                    self.result_table.set_model(Some(model));
                    self.status_label
                        .set_text(&format!("Found {} entries", model.iter_n_children(None)));
                } else {
                    self.status_label.set_text("Executed successfully");
                }
            })
            .unwrap_or_else(|err| {
                self.status_label
                    .set_text(err.message.as_deref().unwrap_or("SQLite Error"));
            });
        } else {
            self.status_label.set_text("No database opened");
        }
    }

    /// Opening a database.
    fn on_open(&mut self) {
        let directory = self.db.as_ref().and_then(|db| db.get_path().parent());
        if let Some(path) = db_open_dialog(self.window.upcast_ref(), directory) {
            self.connect(path);
        }
    }

    /// Creating a new database.
    fn on_new(&mut self) {
        if let Some(path) = db_new_dialog(self.window.upcast_ref()) {
            self.connect(path);
        }
    }

    /// Display the database schema.
    fn on_schema(&self) {
        self.execute("select name, sql from sqlite_master where type=\"table\"");
    }

    /// Execute an user provided sql statement.
    fn on_execute(&self) {
        if let Some(statement) = self
            .command_field
            .get_buffer()
            .and_then(|buf| buf.get_text(&buf.get_start_iter(), &buf.get_end_iter(), false))
            .as_deref()
        {
            self.execute(statement);
            // reload table combobox to admit structural changes
            self.load_tables();
        }
    }

    /// Display the table content of the selected table.
    fn on_table_select(&self) {
        if let Some(selected) = self.table_select.get_active_text() {
            if selected != "<Table>" {
                self.execute(&format!("select * from {}", selected));
            }
        }
    }
}
