use gio::Resource;
use glib::{Bytes, Error};
pub use gtk_resources::UIResource;

/// Registers the gnome resource bundles
pub fn register() -> Result<(), Error> {
    // load the gresource binary at compile time.
    let res_bytes = include_bytes!("../resources/sqlite-utility.gresource");

    let data = Bytes::from(&res_bytes[..]);
    let resource = Resource::from_data(&data)?;
    gio::resources_register(&resource);

    Ok(())
}

#[derive(UIResource, Debug)]
#[resource = "/com/larswrenger/sqlite-utility/menu.xml"]
pub struct UIMenuResource {
    pub menu: gio::MenuModel,
}

#[derive(UIResource, Debug)]
#[resource = "/com/larswrenger/sqlite-utility/about.ui"]
pub struct UIAboutResource {
    pub about_dialog: gtk::AboutDialog,
}

#[derive(UIResource, Debug)]
#[resource = "/com/larswrenger/sqlite-utility/window.ui"]
pub struct UIWindowResource {
    pub main_window: gtk::ApplicationWindow,
    pub header_bar: gtk::HeaderBar,
    pub command_field: gtk::TextView,
    pub table_select: gtk::ComboBoxText,
    pub result_table: gtk::TreeView,
    pub status: gtk::Label,
    pub new: gtk::Button,
    pub open: gtk::Button,
    pub schema: gtk::Button,
    pub execute: gtk::Button,
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn are_resources_complete() {
        gtk::init().unwrap();
        register().unwrap();

        UIMenuResource::load().unwrap();
        UIAboutResource::load().unwrap();
        UIWindowResource::load().unwrap();
    }
}
